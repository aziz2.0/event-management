<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('events');
    }
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    Route::GET('invite-members/{event}', [EventController::class, 'inviteMembers']);
    Route::POST('sent-invititation', [EventController::class, 'sentInvite']);
    Route::DELETE('remove-member/{eventuser}', [EventController::class, 'removeMember']);
    Route::get('events/invitation', [EventController::class, 'invitation']);
    Route::get('accept-invitation/{eventuser}', [EventController::class, 'acceptInvitation']);
    Route::POST('events/search', [EventController::class, 'searchEvent']);
    Route::get('events/search', function () {
        return redirect('events');
    });

    //event resource route
    Route::resource('events', EventController::class);
});
