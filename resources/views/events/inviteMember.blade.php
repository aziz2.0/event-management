@extends('events.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form action="{{ url('sent-invititation') }}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01">Select members</label>
                    </div>
                    <input type="hidden" name="event_id" value="{{$event->id}}">
                    <select name="user_id[]" class="custom-select" id="inputGroupSelect01" multiple>
                        @foreach ($users as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>

        </div>
        <button type="submit" class="btn btn-primary btn-block">Sent Invitation</button>
        </form>
    </div>
    </div>
    @include('events.addevent-modal')
@endsection
