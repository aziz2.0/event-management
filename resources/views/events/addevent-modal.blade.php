<div class="modal fade" id="addEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="{{route('events.store')}}" method="post">
            @csrf
            <div class="form-group">
                <label>Event Name</label>
                <input type="text" class="form-control" name="name" id="name"  placeholder="Enter Event Name">
            </div>
            <div class="form-group">
                <label>Event Description</label>
                <input type="text" class="form-control" name="description" placeholder="Enter Event Description">
            </div>
            <div class="form-group">
                <label>Event Date</label>
                <input type="date" class="form-control" name="event_date" placeholder="Enter Event Date">
            </div>
            <div class="form-group">
                <label>Event Time</label>
                <input type="time" class="form-control" name="event_time" placeholder="Enter Event Time">
            </div>
            <div class="form-group">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-block">Create Event</button>
            </div>
        </form>
      </div>
    </div>
  </div>
