<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Event Management system</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{ route('events.index') }}">Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('events/invitation')}}">Invitations<span class="sr-only">(current)</span>
                        @php
                            $invt = App\Models\EventUser::where('user_id', auth()->user()->id)
                                         ->get();
                        @endphp
                        <span class="badge badge-pill badge-info">{{count($invt)}}</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('dashboard')}}">Dashboard</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0" action="{{url('/events/search')}}" method="POST">
                @csrf
                <input class="form-control mr-sm-2" type="text" placeholder="Search" name="search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <a class="btn btn-sm btn-danger ml-2" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                this.closest('form').submit();">
                    {{ __('Log Out') }}
                </a>
            </form>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Event Management System</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button data-toggle="modal" data-target="#addEvent" class="btn btn-primary m-3 btn-block">Create
                    Event</button>
                @if ($errors->any())
                    @foreach ($errors->all() as $err)
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">{{ $err }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endforeach
                @endif
                @if (session('msg'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        {{ session('msg') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>
        @yield('content')
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>
