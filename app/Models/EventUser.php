<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class EventUser extends Model
{
    use HasFactory, Sortable;
    public $fillable = [
        'event_id',
        'user_id',
        'is_accept',
    ];
    public $sortable = [
        'event_id',
        'user_id',
        'is_accept',
    ];


    public function user()
    {
            return $this->belongsTo(User::class);
    }
    public function event()
    {
            return $this->belongsTo(Event::class);
    }
}
