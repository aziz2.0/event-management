<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Event;
use App\Models\EventUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function index()
    {
        $events = Event::sortable()
            ->where('user_id', auth()->user()->id)
            ->orderBy('created_at', 'DESC')
            ->paginate(3);

        return view('events.index', compact('events'));
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'yesterday' => Carbon::yesterday(),
            'name' => 'required',
            'description' => 'required',
            'event_date' => 'required|date|after:yesterday',
            'event_time' => 'required',
        ]);

        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        Event::create($data);
        return redirect()->route('events.index')->with('msg', 'Event created successfully');
    }

    public function edit(Event $event)
    {
        return $event;
    }

    public function update(Request $request, Event $event)
    {
        //
    }

    public function destroy(Event $event)
    {
        //
        if ($event->user_id == auth()->id()) {
            EventUser::where('event_id', $event->id)->delete();
            $event->delete();
            return redirect()->route('events.index')->with('msg', 'Event deleted successfully');
        } else {
            return redirect()->route('events.index')->with('msg', 'You are not authorized to delete this event');
        }
    }

    public function inviteMembers(Event $event)
    {
        $users = User::where('id', '!=', auth()->id())->get();
        return view('events.inviteMember', compact('users', 'event'));
    }

    public function sentInvite(Request $request)
    {
        $this->validate($request, [
            'event_id' => 'required',
            'user_id' => 'required',
        ]);
        for ($i = 0; $i < count($request->user_id); $i++) {

            $data['user_id'] = $request->user_id[$i];
            $data['event_id'] = $request->event_id;
            EventUser::create($data);
        }
        return redirect()->route('events.index')->with('msg', 'Members Invited successfully');
    }

    public function removeMember(EventUser $eventuser)
    {
        $event = Event::find($eventuser->event_id);
        if ($event->user_id == auth()->user()->id) {
            if ($eventuser->delete()) {
                return redirect()->route('events.index')->with('msg', 'Members Removed successfully');
            }
        }
        return redirect()->back();
    }

    public function invitation()
    {
        $invitation = EventUser::sortable()->where('user_id', auth()->user()->id)->paginate(5);
        return view('events.invitation', compact('invitation'));
    }

    public function acceptInvitation(EventUser $eventuser)
    {
        if ($eventuser->user_id == auth()->user()->id) {
            $eventuser->update(['is_accept' => true]);
            return redirect('events/invitation')->with('msg', 'Invitation Accepted successfully');
        }
        return redirect()->route('events.indexs');
    }

    public function searchEvent(Request $request)
    {
        $query = $request->search;
        if ($query) {
            $events = Event::sortable()
                ->where([
                    ['name', 'like', '%' . $query . '%'],
                    ['user_id', Auth::user()->id],
                ])
                ->orWhere('description', 'like', '%' . $query . '%')
                ->orderBy('id', 'desc')
                ->get();

            return view('events.search', compact('events'));
        }
        return redirect()->back()->with('msg', 'Please enter atleast one value in field');
    }
}
